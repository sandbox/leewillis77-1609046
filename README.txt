This module adds mailcheck (https://github.com/Kicksend/mailcheck) support to
the Drupal Commerce checkout page - to help you get your customer's details
right - first time.

You should already have Drupal Commerce installed, then simply activate the
module, and you're set!


== Frequently Asked Questions ==

* I use shopping cart XYZ - why isn't that supported?
This was developed for a specific need, but it's fairly easy to extend for
other e-commerce solutions. I'm open to accepting patches for other carts, or
to look at integrating them - get in touch at
http://plugins.leewillis.co.uk/contact/ if you have a patch or request.
