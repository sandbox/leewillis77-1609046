jQuery(document).ready(function() {
  jQuery('#commerce-checkout-form-checkout').delegate('#edit-account-login-mail', 'blur', function() {
    jQuery(this).mailcheck({
      suggested: function(element, suggestion) {
        if (jQuery('#decmc-suggest').length < 1) {
          jQuery('div.form-item-account-login-mail').append('<span id="decmc-suggest"></span>');
        }
        jQuery('#decmc-suggest').html("Did you mean "+suggestion.full+"?");
      },
    });
  });
});
